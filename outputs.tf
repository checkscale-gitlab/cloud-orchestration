output "bastion-nat-ip" {
    description = "External IP address of the bastion compute instance"
    value       = google_compute_instance.bastion-instance.network_interface.0.access_config.0.nat_ip
}

output "master-nat-ip" {
    description = "External IP address of the bastion compute instance"
    value       = google_compute_instance.master-instance.network_interface.0.access_config.0.nat_ip
}

output "slave-instance-ip" {
    description = "The internal ip address of the slave instance"
    value       = module.slave-instance.slave-instance-ip
}