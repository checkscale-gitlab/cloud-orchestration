#============
# Bastion node
#============
resource "google_compute_instance" "bastion-instance" {
    name           = "bastion"
    machine_type   = var.machine-type
    can_ip_forward = true
    metadata       = { ssh-keys = var.ssh-key }
    boot_disk {
        initialize_params {
            image = var.machine-image
        }
    }
    network_interface {
        subnetwork = google_compute_subnetwork.bastion-subnet.id
        access_config {}
    }
}

#============
# Master node
#============
resource "google_compute_instance" "master-instance" {
    name           = "master"
    machine_type   = var.machine-type
    can_ip_forward = true
    metadata       = { ssh-keys = var.ssh-key }
    boot_disk {
        initialize_params {
            image = var.machine-image
        }
    }
    network_interface {
        subnetwork = google_compute_subnetwork.master-subnet.id
        access_config {}
    }
}

#=============
# Slave nodes
#=============
module "slave-instance" {
    source       = "./slave/compute"
    subnetwork   = google_compute_subnetwork.slave-subnet.id 
}