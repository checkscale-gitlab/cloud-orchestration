output "slave-instance-ip" {
    description = "The internal ip address of the slave instance"
    value       = google_compute_instance.slave-instance[*].network_interface.0.network_ip 
}